module.exports = {
  rules: {
    'at-rule-name-newline-after': 'always-multi-line',
    'at-rule-semicolon-newline-after': 'always',
    'block-closing-brace-empty-line-before': 'always-multi-line',
    'block-no-empty': null,
    'color-no-invalid-hex': true,
    'comment-empty-line-before': [
      'always',
      {
        ignore: ['stylelint-commands', 'after-comment'],
      },
    ],
    'custom-property-empty-line-before': 'always',
    'declaration-colon-space-after': 'always',
    indentation: [
      'tab',
      {
        except: ['value'],
      },
    ],
    'max-empty-lines': 2,
    'rule-empty-line-before': [
      'always',
      {
        except: ['first-nested'],
        ignore: ['after-comment'],
      },
    ],
    'unit-whitelist': ['em', 'px', '%', 's'],
    'value-list-comma-newline-after': 'always-multi-line',
    'value-list-comma-newline-before': 'always-multi-line',
    'value-list-comma-space-after': 'always-single-line',
    'value-list-comma-space-before': 'always-single-line',
  },
}
