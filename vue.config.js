const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: '',

  // css相关配置
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: true,
    // 开启 CSS source maps?
    sourceMap: false,
    // 启用 CSS modules for all css / pre-processor files.
    requireModuleExtension: true,
  },

  chainWebpack: config => {
    config.resolve.alias
      .set('~styles', resolve('src/assets/styles'))
      .set('~scripts', resolve('src/assets/scripts'))
  },

  pluginOptions: {
    cordovaPath: 'src-cordova',
  },
}
