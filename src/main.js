import Vue from 'vue'
import App from './App.vue'
import router from '@/routers'

import IonicVue from '@ionic/vue'
import '@ionic/core/css/core.css'
import '@ionic/core/css/ionic.bundle.css'
import store from './store'

Vue.config.productionTip = false

Vue.use(IonicVue)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
