const crypto = require('crypto')
const storage = require('store')

export const appDevices = storage.get('device') || {
  deviceId: 'test',
  clientId: 'test',
  deviceName: 'test',
  deviceVersion: '7.0.0',
  deviceAppVersion: '6.2.0',
}

export const nowTimeStamp = Math.floor(new Date().getTime() / 1000)

export const ENCRYPT_KEY = 'GL_SALT_MD5_KEY'
export const __VERSION__ = '1.0.0'
export const MD5Key = password =>
  crypto
    .createHash('md5')
    .update(ENCRYPT_KEY + password.toString())
    .digest('hex')

export const getDeviceIdAndClientId = () => {
  if (true || process.env.NODE_ENV === 'development') {
    let device = {
      deviceId: 'test',
      clientId: 'test',
      deviceName: 'test',
      deviceVersion: '7.0.0',
      deviceAppVersion: '6.2.0',
    }
    storage.set('device', device)
  } else {
    // TODO: 调用 cordova插件读取设备信息
  }
}
