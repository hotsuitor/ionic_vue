import bsRequest from './base'
const storage = require('store')

export function checkLogin(params) {
  if (params.token) {
    return true
  }
}

export const login = data => {
  return bsRequest.bsRequest().then(axios => {
    axios
      .post('api/agent/login', {
        deviceName: 'iphone6',
        deviceVersion: '6.2.0',
        username: data.username,
        password: data.password,
        apiVersion: '0.0.1',
      })
      .then(res => {
        let opt = res || {}
        if (opt.id == '00') {
          console.log('该账号不能登录')
          return
        }

        storage.set('lastUser', opt.id)
        storage.set('userInfo', opt)
        return Promise.resolve(res)
      })
      .catch(err => Promise.reject(err))
  })
}
