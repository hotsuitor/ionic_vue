import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    redirect: '/home',
  },
  {
    name: 'home',
    component: () => import('@/pages/Home'),
    mate: {
      title: '首页',
    },
  },
  {
    name: 'user',
    component: () => import('@/pages/user/Index'),
    mate: {
      title: '个人中心',
    },
  },
]

// add route path
routes.forEach(route => {
  route.path = route.path || '/' + (route.name || '')
})

const router = new VueRouter({ routes })

export default router
