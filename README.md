# ionic-vue

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```


git hook postcss 该命令会修改src下的源码，取消改配置
```
  "postcss --config postcss.config.js --replace",
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## 目录结构

```
├─.editorconfig # 编辑器文件格式统一插件的配置文件
├─.prettierrc # 代码格式化配置文件
├─babel.config.js # babel配置文件
├─package.json
├─postcss.config.js # postcss配置文件
├─README.md # 了解一个项目前，看我
├─stylelint.config.js # css、scss样式文件格式化插件
├─vue.config.js # vue配置文件，webpack配置也在这里
├─yarn.lock # yarn 包管理工具锁文件
├─src
|  ├─App.vue
|  ├─main.js
|  ├─utils # js工具目录
|  ├─store
|  |   ├─actions.js
|  |   ├─getters.js
|  |   ├─index.js
|  |   ├─muations.js
|  |   └muations_type.js # muations定义常量配置文件
|  ├─routers
|  |    └index.js
|  ├─pages
|  |   └Home.vue
|  ├─components
|  |     └HelloIonic.vue
|  ├─assets
|  |   ├─styles
|  |   |   └hello.css
|  |   ├─scripts
|  |   ├─img
|  |   ├─fonts
|  ├─api
|  |  └base.js
├─public
|   ├─favicon.ico
|   └index.html
```

## 开发规范

遵循 vue[风格指南](https://cn.vuejs.org/v2/style-guide/)

### 文件命名规范：

-   普通文件采用小写+下划线方式命名，eg: `config_url.json`；
-   单文件组件名采用单词大写开头 (PascalCase)命名，eg: `HelloIonic.vue`；
-   在单文件组件和字符串模板中组件名应该总是 PascalCase 的——但是在 DOM 模板中(即`<template>`模板中)总是 kebab-case 的，eg: `<hello-ionic></hello-ionic>`;
-   在单文件组件、字符串模板和 JSX 中没有内容的组件应该是自闭合的——但在 DOM 模板里永远不要这样做，eg: `<my-component></my-component>`

### 编码规范

-   编辑器统一风格插件 editorconfig 配置文件是根目录下的 `.editorconfig`
-   编码风格统一化插件 prettier 配置文件是根目录下的 `.prettierrc`

> 请务必安装以上推荐的插件，提交代码会有 commit 钩子校验代码格式，不通过将会提交失败

### 注意事项

不要把在开发境中用到依赖包安装在运行环境，用以下命令安装开发环境依赖包：

```bash
npm install package-name -D
#or
yarn add package-name -D
```

### 组件选项的顺序

组件选项应该有统一的顺序。

```js
export default {
    name: '',

    mixins: [],

    components: {},

    props: {},

    data() {},

    computed: {},

    watch: {},

    created() {},

    mounted() {},

    destroyed() {},

    methods: {},
}
```

## 扩展配置

### postcss 适配移动端

配置 postcss.config.js 文件
配置 px to rem

```js
  plugins: [
    require('autoprefixer'), // 浏览器兼容配置
    require('postcss-nested'), // 普通css也可以嵌套写css
    require('postcss-pxtorem')({ // px转rem插件
      rootValue: 16,
      propList: ['*'],
    }),
  ],
```

> 配置了 postcss-nested 插件，css 文件可以写嵌套样式

### css 配置

配置 vue.config.js 文件

```js
 // css相关配置
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: true,
    // 开启 CSS source maps?
    sourceMap: false,
    // 启用 CSS modules for all css / pre-processor files.
    requireModuleExtension: true,
  },
```

### 配置 git commit 钩子

1. 安装 `npx mrm lint-staged`
   安装完成会自动在 `package.json` 配置执行的钩子命令
2. 配置格式化命令，需要分别配置不同类型文件解释器，不然 prettier 会按照全局解析器格式化文件
   [参考](https://zhuanlan.zhihu.com/p/87586114)

### 代理配置

配置 vue.config.js 文件

```js
devServer: {
  proxy: {
    '/api': {
      target: 'http://localhost:9999/',
      ws: true,
      changeOrigin: true,
      pathRewrite: {
        '^/api': ''
      }
    }
  }
},
```

## cordova 引入

安装 vue cordova 插件 `vue-cli-plugin-cordova`

```
vue add cordova
```

## 实际使用

### 使用 cordova 开发混合 app

1. 安装 cordova 插件 `vue add cordova`
2. 参考 [vue-cli-plugin-cordova](https://www.npmjs.com/package/vue-cli-plugin-cordova) 用法

### axios 业务封装

根据实际业务需求，修改封装的业务请求模块 `api/base.js`，修改登录求情模块 `api/login.js`

## 下一步

-   [x] 路由的基本配置
-   [x] 请求库的 axio 的引入配置
-   [x] store 的基本配置
-   [x] 引入 cordova 打包 app
    -   [ ] cordova 打 app 包有问题
-   [ ] 骨架屏
-   [ ] 配置不同环境打包
-   [ ] 打包体积优化
